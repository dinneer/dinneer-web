Rails.application.routes.draw do

  devise_for :admins, class_name: "Admin", controllers: { sessions: "admin/sessions" }
  devise_for :users, class_name: "User", controllers: { omniauth_callbacks: "omniauth_callbacks", 
  	registrations: "registrations", sessions: "sessions" }

  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'

  root to: "home#index"

  resources :foods do
  	member do
  		get :manage_dish
      get :price_dish
      get :ad_dish
      get :will_be_served_dish
      get :photos_dish
      get :about_place_dish
      get :ads_dish
      get :location_dish
  	end
  end

end
