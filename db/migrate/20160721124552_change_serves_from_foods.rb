class ChangeServesFromFoods < ActiveRecord::Migration
  def self.up
  	change_column :foods, :serves, :integer
  end

  def self.down
  	change_column :foods, :serves, :string  	
  end
end
