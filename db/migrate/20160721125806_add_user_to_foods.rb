class AddUserToFoods < ActiveRecord::Migration
  def self.up
  	add_reference :foods, :user, index: true, foreign_key: true
  end

  def self.down
  	remove_reference :foods, :user, index: true, foreign_key: true
  end
end
