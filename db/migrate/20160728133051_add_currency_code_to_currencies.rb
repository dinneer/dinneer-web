class AddCurrencyCodeToCurrencies < ActiveRecord::Migration
  def self.up
    add_column :currencies, :currency_code, :string
  end

  def self.down
    remove_column :currencies, :currency_code
  end
end
