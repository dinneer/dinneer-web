class AddCompletedStepsToFoods < ActiveRecord::Migration
  def self.up
  	add_column :foods, :completed_steps, :string
  end

  def self.down
  	remove_column :foods, :completed_steps
  end
end
