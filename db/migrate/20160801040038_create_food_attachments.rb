class CreateFoodAttachments < ActiveRecord::Migration
  def change
    create_table :food_attachments do |t|
      t.string :photo
      t.boolean :cover, default: false
      t.references :food, index: true
  		
  	  t.timestamps null: false
    end

    add_foreign_key :food_attachments, :foods
  end
end
