class AddCurrencyToFoods < ActiveRecord::Migration
  def self.up
  	add_reference :foods, :currency, index: true, foreign_key: true
  end

  def self.down
  	remove_reference :foods, :currency, index: true, foreign_key: true
  end
end
