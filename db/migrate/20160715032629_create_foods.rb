class CreateFoods < ActiveRecord::Migration
  def change
    create_table :foods do |t|
    	t.string :meal
    	t.string :serves
    	t.string :location
    	t.string :available
    	t.date :initial_available
    	t.date :final_available
    	t.string :verification, default: 'pending'
    	t.decimal :price, precision: 10, scale: 2
    	t.string :currency
    	t.string :title
    	t.text :description
    	t.text :appetizer
    	t.text :main_course
    	t.text :final_dish
    	t.text :final_dish
    	t.string :in_place
    	t.time :starts_at
    	t.time :ends_at

    	t.timestamps null: false
    end
  end
end
