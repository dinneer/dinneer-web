# encoding: utf-8

class FoodAttachmentUploader < CarrierWave::Uploader::Base
  include CarrierWave::MiniMagick

  def store_dir
    "uploads/#{Rails.env}/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  # def default_url
	#   ActionController::Base.helpers.asset_path("food_placeholder.svg")
	# end

	version :thumb do
		process resize_to_fill: [200, 200]
	end

	version :medium do
		process resize_to_fill: [800, 800]
	end

	def extension_white_list
		%w(jpg jpeg png gif)
	end

	def content_type_whitelist
	  /\Aimage\/.*\Z/
	end

	def filename
    "#{secure_token}.#{file.extension}" if original_filename.present?
  end

  protected

  	def secure_token
	    var = :"@#{mounted_as}_secure_token"
	    model.instance_variable_get(var) || model.instance_variable_set(var, SecureRandom.uuid)
	  end
end
