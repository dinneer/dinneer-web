class Food < ActiveRecord::Base
	extend Enumerize

	belongs_to :user
	belongs_to :currency

	has_many :food_attachments, dependent: :destroy

	enumerize :meal, in: [:breakfast, :lunch, :dinner]	
	enumerize :available, in: [:ever, :sometimes]

	serialize :completed_steps, Array 
end