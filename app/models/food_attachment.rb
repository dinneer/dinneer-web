class FoodAttachment < ActiveRecord::Base
  mount_uploader :photo, FoodAttachmentUploader

  belongs_to :food
end