module ApplicationHelper
	def decorate(object)
	  ActiveDecorator::Decorator.instance.decorate(object)
	end

	def body_class(controller)
		if controller.in?("home")
			return "home"
		else
			return "application"
		end
	end

	def menu_tab_class(current_controller, tabs)
		tabs = Array(tabs)
		return "active" if tabs.include?(current_controller)
	end
end
