class RegistrationsController < Devise::RegistrationsController
  prepend_before_filter :require_no_authentication, only: [:new, :create, :cancel]
  prepend_before_filter :authenticate_scope!, only: [:edit, :update, :destroy]

  # GET /resource/sign_up
  def new
    if session[:facebook_uid].present?
      build_resource({ name: session[:user_name], email: session[:user_email], 
        avatar_id: session[:user_avatar_id], facebook_uid: session[:facebook_uid] })
    else
      build_resource({})
    end
    
    set_minimum_password_length
    yield resource if block_given?

    respond_to do |format|
      format.js
      format.html do 
        render "new", resource: self.resource, resource_name: "user"
      end
    end
  end

  # POST /resource
  def create
    @resource = User.new(sign_up_params)

    if @resource.save
      # feed = Feed.new(@resource)
      # @feed_items = feed.items.page(params[:page]).per(5)

      session.delete(:user_name)
      session.delete(:user_email)
      session.delete(:avatar_id)
      session.delete(:facebook_uid)

      sign_in(@resource)

      respond_to do |format|
        format.js
        format.html { redirect_to home_index_path }
      end
    else
      @error = @resource.errors.full_messages[0]
      clean_up_passwords @resource

      respond_to do |format|
        format.js
        format.html { render 'new', locals: { resource: @resource } }
      end
    end
  end

  # GET /resource/edit
  def edit
    @resource = resource
    @city = @resource.city
    @image = @resource.image

    respond_to do |format|
      format.js
      format.html do 
        render '_edit', locals: { resource: @resource, resource_name: resource_name, 
          city: @city, image: @image }
      end
    end
  end

  # PUT /resource
  # We need to use a copy of the resource because we don't want to change
  # the current user in place.
  def update
    @resource = current_user

    if account_update_params[:password].blank? && account_update_params[:password_confirmation].blank?
      account_update_params.delete(:password)
      account_update_params.delete(:password_confirmation)
    end

    if @resource.update_attributes(account_update_params)
      @resource.image = UserImage.find_by(id: @resource.avatar_id) if @resource.avatar_id.present?
      @city = @resource.city

      if @user.real_estate_broker?
        @real_estates = @resource.real_estates
      else
        @enterprises = @resource.enterprises
      end

      @followers = @resource.followers.includes(:image)
      @blacklist_ids = @resource.blocked_users.where(id: @followers.ids).pluck(:blocked_user_id)
      @friends = @resource.my_friends

      sign_in(@resource, bypass: true)

      respond_to do |format|
        format.js
        format.html { redirect_to me_users_path }
      end
    else
      @error = @resource.errors.full_messages[0]
      @city = @resource.city
      @image = @resource.image

      respond_to do |format|
        format.js
        format.html do 
          render '_edit', locals: { resource: @resource, resource_name: "user", city: @city, 
            image: @image }
        end
      end
    end
  end

  # DELETE /resource
  def destroy
    resource.destroy
    Devise.sign_out_all_scopes ? sign_out : sign_out(resource_name)
    set_flash_message :notice, :destroyed if is_flashing_format?
    yield resource if block_given?
    respond_with_navigational(resource){ redirect_to after_sign_out_path_for(resource_name) }
  end

  # GET /resource/cancel
  # Forces the session data which is usually expired after sign
  # in to be expired now. This is useful if the user wants to
  # cancel oauth signing in/up in the middle of the process,
  # removing all OAuth session data.
  def cancel
    expire_data_after_sign_in!
    redirect_to new_registration_path(resource_name)
  end

  protected

  def update_needs_confirmation?(resource, previous)
    resource.respond_to?(:pending_reconfirmation?) &&
      resource.pending_reconfirmation? &&
      previous != resource.unconfirmed_email
  end

  # By default we want to require a password checks on update.
  # You can overwrite this method in your own RegistrationsController.
  def update_resource(resource, params)
    resource.update_with_password(params)
  end

  # Build a devise resource passing in the session. Useful to move
  # temporary session data to the newly created user.
  def build_resource(hash=nil)
    self.resource = resource_class.new_with_session(hash || {}, session)
  end

  # Signs in a user on sign up. You can overwrite this method in your own
  # RegistrationsController.
  def sign_up(resource_name, resource)
    sign_in(resource_name, resource)
  end

  # The path used after sign up. You need to overwrite this method
  # in your own RegistrationsController.
  def after_sign_up_path_for(resource)
    after_sign_in_path_for(resource)
  end

  # The path used after sign up for inactive accounts. You need to overwrite
  # this method in your own RegistrationsController.
  def after_inactive_sign_up_path_for(resource)
    scope = Devise::Mapping.find_scope!(resource)
    router_name = Devise.mappings[scope].router_name
    context = router_name ? send(router_name) : self
    context.respond_to?(:root_path) ? context.root_path : "/"
  end

  # The default url to be used after updating a resource. You need to overwrite
  # this method in your own RegistrationsController.
  def after_update_path_for(resource)
    signed_in_root_path(resource)
  end

  # Authenticates the current scope and gets the current resource from the session.
  def authenticate_scope!
    send(:"authenticate_#{resource_name}!", force: true)
    self.resource = send(:"current_#{resource_name}")
  end

  def sign_up_params
    devise_parameter_sanitizer.sanitize(:sign_up)
  end

  def account_update_params
    devise_parameter_sanitizer.sanitize(:account_update)
  end

  def translation_scope
    'devise.registrations'
  end
end