class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_filter :configure_permitted_parameters, if: :devise_controller?
  before_filter :store_current_location
  before_filter :store_current_location, unless: :devise_controller?

  before_action :set_locale

  respond_to :html, :js

  layout 'application'

  def render_unauthorized
    if request.headers["HTTP_AUTHENTICATION_TOKEN"].present?
      authenticate_user_from_token
    else
      unless user_signed_in?
        session[:sign_in_redirect_url] = request.original_url

        redirect_to new_user_session_path
      end
    end
  end

  def authenticate_user_from_token
    if user_signed_in?
      cookies[:authentication_token] = current_user.authentication_token
    else
      authentication_token = request.headers["HTTP_AUTHENTICATION_TOKEN"]

      if authentication_token.blank?
        cookies.delete(:authentication_token)
        return false
      end

      user = User.find_by(authentication_token: authentication_token)

      if user && Devise.secure_compare(user.authentication_token, authentication_token)
        cookies[:authentication_token] = user.authentication_token
        
        sign_in(user)
      end
    end
  end

  def set_locale
    locale = request.headers[:locale]
    (I18n.locale = locale.present? ? locale.gsub(/\A"/m, "").gsub(/"\Z/m, "") : "pt-BR") rescue nil
  end

  private

    def configure_permitted_parameters
      user_attributes = [
        :user_type, :name, :email, :city_id, :phone, :profile_description,
        :avatar_id, :person_type, :state_id, :facebook_uid
      ]

      devise_parameter_sanitizer.for(:sign_up) << user_attributes
      devise_parameter_sanitizer.for(:account_update) << user_attributes
    end

    def store_current_location
      store_location_for(:user, request.url)
    end

    def after_sign_out_path_for(resource)
      request.referrer || root_path
    end
end
