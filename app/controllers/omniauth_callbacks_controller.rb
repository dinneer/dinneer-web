class OmniauthCallbacksController < Devise::OmniauthCallbacksController

  def all
    @user = User.from_omniauth(request.env["omniauth.auth"])

    if @user.persisted?
      #The line below will throw if @user is not activated
      sign_in_and_redirect @user, :event => :authentication
      if is_navigational_format?
        set_flash_message(:notice, :success, :kind => "Facebook") 
      end
    else
      session["devise.facebook_data"] = request.env["omniauth.auth"]
      redirect_to new_user_registration_url
    end
  end

  def failure
    super
  end

  alias_method :facebook, :all

end
