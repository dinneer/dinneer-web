class FoodsController < ApplicationController
	before_filter :verify_authenticity_token, except: :new
  before_filter :render_unauthorized, except: :new
  before_filter :authenticate_user!, except: :new

	def new
		@food = current_user.foods.build if current_user.present?
		@food = "" if current_user.blank?
	end

	def create
		@food = current_user.foods.create(food_params)
		render manage_dish_food_path(@food), local: ""
	end

	def edit
		@food = current_user.foods.find_by(id: params[:id])
	end

	def show
		@food = Food.find_by(id: params[:id])
	end

	def update
		@food = current_user.foods.find_by(id: params[:id])

		@food.update(food_params)

		if @food.save
			case validate_params[:step].to_i	
				when 1
					respond_with @food, location: price_dish_food_path(@food)
				when 2
					respond_with @food, location: ad_dish_food_path(@food)
				when 3
					respond_with @food, location: will_be_served_dish_food_path(@food)
				when 4
					respond_with @food, location: photos_dish_food_path(@food)
				when 5
					FoodAttachment.create(photo: photo_params[:photo], food_id: @food.id)
					respond_with @food, location: photos_dish_food_path(@food)
			end

		else
			respond_with @food, location: manage_dish_food_path(@food)
			flash[:error] = "Erro ao tentar salvar."
		end
		
	end

	def manage_dish
		@food = current_user.foods.find_by(id: params[:id])
	end

	def price_dish
		@food = current_user.foods.find_by(id: params[:id])
	end

	def ad_dish
		@food = current_user.foods.find_by(id: params[:id])
	end

	def will_be_served_dish
		@food = current_user.foods.find_by(id: params[:id])
	end

	def photos_dish
		@food = current_user.foods.find_by(id: params[:id])
	end

	def about_place_dish
		@food = current_user.foods.find_by(id: params[:id])
	end

	def ads_dish
		@food = current_user.foods.find_by(id: params[:id])
	end

	def location_dish
		@food = current_user.foods.find_by(id: params[:id])
	end

	private
		def food_params
			params.require(:food).permit(:meal, :serves, :location, :initial_available, 
				:final_available, :verification, :price, :currency, :title, :description, 
				:appetizer, :main_course, :final_dish, :final_dish, :in_place, :starts_at, 
				:ends_at, :available, :price, :currency_id, :completed_steps)
		end

		def validate_params
			params.require(:food).permit(:step)
		end

		def photo_params
			params.require(:food).permit(:photo, :cover, :food_id)
		end

end